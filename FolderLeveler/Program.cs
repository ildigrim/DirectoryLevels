﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FolderLeveler
{
    class Program
    {
        static void Main(string[] args)
        {
            Level level = new Level();
            Directory.CreateDirectory("Levels");            
            for (; ; )
            {
                Console.Clear();
                Console.WriteLine("Удалить или добавить? (del,add)");
                switch (Console.ReadLine())
                {
                    case "add":
                        try
                        {
                            level.LevelDetermine();
                            Console.WriteLine("Введите количество добавляемых уровней:");
                            level.LevelAdd(level.Current, Int32.Parse(Console.ReadLine()));                             
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message+"\nНажмите любую клавишу для продолжения.");                            
                            Console.ReadKey();
                        }
                        break;
                    case "del":
                        level.LevelDetermine();
                        Console.WriteLine("Введите количество удаляемых уровней:");                        
                        level.LevelDel(level.Current, Int32.Parse(Console.ReadLine()));                             
                        break;
                }
            }
        }
    }
}
