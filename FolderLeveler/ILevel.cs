﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderLeveler
{
    interface ILevel
    {        
        void LevelAdd(int L, int l);
        void LevelDel(int L, int l);
        int LevelDetermine();
    }
}
