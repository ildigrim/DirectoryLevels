﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FolderLeveler
{
    class Level : ILevel
    {
        const int _levelMax = 10;                       //Ограничитель уровней
        string path = "Levels";                         //Имя корня
        string temp;                                    //текущий путь
        string fTemp;
        int current;                                    //Текущий уровень
        public int Current
        {
            get { return current; }
        }

        public void LevelAdd(int L, int l)              //Добавление l уровней
        {
            if (l > 0)
            {
                if (L < _levelMax) 
                {
                    L++;
                    fTemp =temp+ $"\\file_level_{L}.txt";
                    FileStream fstream = new FileStream(fTemp, FileMode.Create);                    
                    fstream.Close();                    
                    temp += $"\\Level_{L}";                                                          
                    Directory.CreateDirectory(temp);                    
                    l--;                    
                    LevelAdd(L, l);
                }
                else { throw new Exception("Превишен лимит уровней."); }
            }
        }

        public void LevelDel(int L, int l)             //Удаление l уровней
        {
            L++; 
            L -= l;            
            for (int i = 0; i <= L; i++)
            {
                if (i == L)
                {
                    path += $"\\file_level_{i}.txt";
                    File.Delete(path);
                    path = path.Substring(0, i < 10?path.Length - 17: path.Length - 18);
                }
                path += $"\\Level_{i}";                
            }           
            Directory.Delete(path,true);            
            path = "Levels";           
        }

        int p = 0;

        public int LevelDetermine()                    //Определение текущего уровня
        {            
            string[] dirs = Directory.GetDirectories(path);
            if (dirs.Length != 0)
            {                
                path += $"\\Level_{p}";
                p++;                
                return LevelDetermine();
            }            
            temp = "";            
            temp = temp.Insert(0, path);         
            path = "Levels";
            current = p-1;
            p = 0;
            return current;            
        }
    }
}
